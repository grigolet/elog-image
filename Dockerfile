FROM ubuntu:latest

MAINTAINER Gianluca Rigoletti <gianluca.rigoletti@cern.ch>

RUN apt-get update -q && \
    apt-get install -y \
    openssl \
    imagemagick \ 
    ghostscript \
    elog \
    wget \
    rsync \
    tzdata \
    unzip && \
    apt-get clean

# RUN wget https://download.cksource.com/CKEditor/CKEditor/CKEditor%204.11.3/ckeditor_4.11.3_full_easyimage.zip && \
#     unzip ckeditor_4.11.3_full_easyimage.zip && \
#     rm -rf /usr/share/elog/scripts/ckeditor && \
#     mv ckeditor /usr/share/elog/scripts
    
RUN chgrp -R 0 /var/lib/elog && \
    chmod -R g=u /var/lib/elog

ENV TZ=Europe/Rome
RUN ln -snf /usr/share/zoneinfo/$TZ /etc/localtime && echo $TZ > /etc/timezone
EXPOSE 8080
USER 10001

CMD ["elogd", "-p", "8080", "-c", "/home/elogd.cfg"]